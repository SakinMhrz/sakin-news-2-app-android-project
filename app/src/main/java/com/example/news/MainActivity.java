package com.example.news;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Response;
import com.example.news.parameter.Articles;
import com.example.news.parameter.Headlines;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {
RecyclerView recyclerView;
MainAdapter adapter;
final String API_KEY="YOUR API KEY";
Button refreshButton;
List<Articles> articles=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.recycler);
        refreshButton=findViewById(R.id.refresh);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final String country=getCountry();
        fetchJSON(country,API_KEY);
    }

    private void fetchJSON(String country, String api_key) {
        retrofit2.Call<Headlines> call = Client.getInstance().getApi().getHeadlines(country,api_key);
        call.enqueue(new Callback<Headlines>() {
            @Override
            public void onResponse(retrofit2.Call<Headlines> call, retrofit2.Response<Headlines> response) {
                if (response.isSuccessful()) {
                    articles.clear();
                    articles.addAll(response.body().getArticles());
                    adapter=new MainAdapter(MainActivity.this,articles);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Headlines> call, Throwable t) {
                Log.d("Error", t.getLocalizedMessage());
            }
        });
    }

    private String getCountry() {
        Locale locale=Locale.getDefault();
        String country=locale.getCountry();
        return country.toLowerCase();
    }
}
